public class Task1_1 {
    public static String print(int n) {
        //for(int i = 1; i<n; i++){
        if (n == 1) {
            return "1";
        }
        return print(n - 1) + " " + n;

    }

    public static void main(String[] args) {
        System.out.println(print(5));
    }
}

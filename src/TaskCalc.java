import java.util.Scanner;

public class TaskCalc {
    static Scanner scan = new Scanner(System.in);

    public static int getInt() {
        System.out.println("Введите целое число (!=0): ");
        int num = scan.nextInt();
//        System.out.println("Введите число 2: ");
//        int num2 = scan.nextInt();
//        System.out.println("Ваши числа: " + num1 + " и " + num2);
        return num;
    }

    public static char getOperation() {
        System.out.println("Выберите операцию: +, -, * или /: ");
        char c = scan.next().charAt(0);
        System.out.println("Вы выбрали операцию: " + c);
        return c;

    }

    public static double calc(int num1, int num2, char c) {
        double res = 0;
        switch (c){
            case '+':
                res=num1+num2;
                break;
            case '-':
                res=num1-num2;
                break;
            case '*':
                res=num1*num2;
                break;
            case '/':
                double num3 = num1;
                double num4 = num2;
                res=(num3/num4);

                break;
                default:
                    System.out.println("Проверьте правильность ввода данных!");
        }
        //System.out.println(res);
        return res;
    }


    public static void main(String[] args) {
        int num1 = getInt();
        int num2 = getInt();
        char c = getOperation();
        double res = calc(num1, num2, c);
        System.out.println("ИТОГО: "+res);

    }
}
